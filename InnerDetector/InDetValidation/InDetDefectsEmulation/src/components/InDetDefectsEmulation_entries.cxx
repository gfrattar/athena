/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "../PixelDefectsEmulatorCondAlg.h"
#include "../PixelDefectsEmulatorAlg.h"

DECLARE_COMPONENT( InDet::PixelDefectsEmulatorCondAlg )
DECLARE_COMPONENT( InDet::PixelDefectsEmulatorAlg )

