/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
 */
/**
 * @file DataModelTestDataCommon/src/PLinksAuxInfo_v1.cxx
 * @author scott snyder <snyder@bnl.gov>
 * @date Jan, 2024
 * @brief For testing packed links.
 */


#include "DataModelTestDataCommon/versions/PLinksAuxInfo_v1.h"


namespace DMTest {


PLinksAuxInfo_v1::PLinksAuxInfo_v1()
  : xAOD::AuxInfoBase()
{
}


} // namespace DMTest
