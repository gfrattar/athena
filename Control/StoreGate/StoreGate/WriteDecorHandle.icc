/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file StoreGate/WriteDecorHandle.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Apr, 2017
 * @brief Handle class for adding a decoration to an object.
 */


#include "CxxUtils/checker_macros.h"
#include "StoreGate/exceptions.h"


namespace SG {


/**
 * @brief Constructor from a WriteDecorHandleKey.
 * @param key The key object holding the clid/key/store/attr.
 *
 * This will raise an exception if the StoreGate key is blank,
 * or if the event store cannot be found.
 */
template <class T, class D>
WriteDecorHandle<T, D>::WriteDecorHandle (const WriteDecorHandleKey<T>& key)
  : Base (key.contHandleKey(), nullptr),
    m_decorKey (key.key()),
    m_acc (SG::decorKeyFromKey (key.key())),
    m_renounced (key.renounced()),
    m_decorated (false)
{
}


/**
 * @brief Constructor from a ReadDecorHandleKey and an explicit event context.
 * @param key The key object holding the clid/key.
 * @param ctx The event context.
 *
 * This will raise an exception if the StoreGate key is blank,
 * or if the event store cannot be found.
 *
 * If the default event store has been requested, then the thread-specific
 * store from the event context will be used.
 */
template <class T, class D>
WriteDecorHandle<T, D>::WriteDecorHandle (const WriteDecorHandleKey<T>& key,
                                          const EventContext& ctx)
  : Base (key.contHandleKey(), &ctx),
    m_decorKey (key.key()),
    m_acc (SG::decorKeyFromKey (key.key())),
    m_renounced (key.renounced()),
    m_decorated (false)
{
}


/**
 * @brief Copy constructor.
 */
template <class T, class D>
WriteDecorHandle<T, D>::WriteDecorHandle (const WriteDecorHandle& rhs)
  : Base (rhs),
    m_decorKey (rhs.m_decorKey),
    m_acc (rhs.m_acc),
    m_renounced (rhs.m_renounced),
    m_decorated (rhs.m_decorated)
{
}


/**
 * @brief Move constructor.
 */
template <class T, class D>
WriteDecorHandle<T, D>::WriteDecorHandle (WriteDecorHandle&& rhs)
  : Base (std::move (rhs)),
    m_decorKey (std::move (rhs.m_decorKey)),
    m_acc (std::move (rhs.m_acc)),
    m_renounced (rhs.m_renounced),
    m_decorated (rhs.m_decorated)
{
  rhs.m_decorated = false;
}


/**
 * @brief Destructor.  This will lock the decoration.
 */
template <class T, class D>
WriteDecorHandle<T, D>::~WriteDecorHandle()
{
  // Lock the decoration.  But don't do anything if we haven't touched it.
  if (m_decorated) {
    const IConstAuxStore* store = this->vectorData()->getConstStore();
    if (store) {
      // Lock the decoration and clear its entry in the cache.
      // Casting away const should be ok because no other thread should
      // be looking at this decoration yet.
      auto avd_nc ATLAS_THREAD_SAFE = const_cast<SG::AuxVectorData*> (this->vectorData());
      avd_nc->lockDecoration (auxid());
    }
  }
}


/**
 * @brief Assignment operator.
 */
template <class T, class D>
WriteDecorHandle<T, D>& WriteDecorHandle<T, D>::operator= (const WriteDecorHandle& rhs)
{
  if (this != &rhs) {
    *static_cast<Base*>(this) = rhs;
    m_acc = rhs.m_acc;
    m_decorKey = rhs.m_decorKey;
    m_renounced = rhs.m_renounced;
    m_decorated = rhs.m_decorated;
  }
  return *this;
}


/**
 * @brief Move operator.
 */
template <class T, class D>
WriteDecorHandle<T, D>& WriteDecorHandle<T, D>::operator= (WriteDecorHandle&& rhs)
{
  if (this != &rhs) {
    *static_cast<Base*>(this) = std::move (rhs);
    m_acc = std::move (rhs.m_acc);
    m_decorKey = std::move (rhs.m_decorKey);
    m_renounced = rhs.m_renounced;
    m_decorated = rhs.m_decorated;
    rhs.m_decorated = false;
  }
  return *this;
}


/**
 * @brief Is the referenced container present in SG?
 *
 * Note that this tests for the presence of the _container_,
 * not for the decoration.
 *
 * Const method; the handle does not change as a result of this.
 */
template <class T, class D>
bool WriteDecorHandle<T, D>::isPresent() const
{
  return Base::isPresent();
}


/**
 * @brief Explicitly set the event store.
 * @param store The new event store.
 *
 * This implicitly does a reset().
 *
 * We need to override this so that the setting gets made on the container
 * handle as well.
 */
template <class T, class D>
StatusCode WriteDecorHandle<T, D>::setProxyDict (IProxyDict* store)
{
  m_decorated = false;
  return Base::setProxyDict (store);
}


/**
 * @brief Fetch the variable for one element, as a reference.
 * @param e The element for which to fetch the variable.
 */
template <class T, class D>
typename WriteDecorHandle<T, D>::reference_type
WriteDecorHandle<T, D>::operator() (const AuxElement& e)
{
  // Ensure that the element we're given is actually an element
  // of the container we're writing.
  if (e.container() != this->vectorData()) {
    if (!m_renounced) {
      throwExcBadDecorElement (Gaudi::DataHandle::Writer,
                               this->clid(),
                               this->m_decorKey);
    }
  }
  else if (!m_decorated) {
    makeDecor (e.container());
  }
  return m_acc (e);
}


/**
 * @brief Fetch the variable for one element, as a reference.
 * @param index The index of the desired element.
 *
 * This looks up the variable in the object referenced by this handle.
 * For a standalone object, pass an index of 0.
 */
template <class T, class D>
typename WriteDecorHandle<T, D>::reference_type
WriteDecorHandle<T, D>::operator() (size_t i)
{
  const AuxVectorData* avd = this->vectorData();
  if (!m_decorated) makeDecor (avd);
  return m_acc (*avd, i);
}


/**
 * @brief Get a pointer to the start of the auxiliary data array,
 *        for the referenced object.
 */
template <class T, class D>
template <class POINTER_TYPE /*= container_pointer_type*/,
          typename /*= std::enable_if_t<!std::is_void_v<POINTER_TYPE> >*/ >
POINTER_TYPE
WriteDecorHandle<T, D>::getDecorationArray()
{
  const AuxVectorData* avd = this->vectorData();
  if (!m_decorated) makeDecor (avd);
  return m_acc.getDecorationArray (*avd);
}


/**
 * @brief Get a span over the auxilary data array,
 *        for the referenced object.
 */
template <class T, class D>
auto
WriteDecorHandle<T, D>::getDecorationSpan() -> span
{
  const AuxVectorData* avd = this->vectorData();
  if (!m_decorated) makeDecor (avd);
  return m_acc.getDecorationSpan (*avd);
}


/**
 * @brief Test to see if this variable exists in the store,
 *        for the referenced object.
 */
template <class T, class D>
inline
bool WriteDecorHandle<T, D>::isAvailable()
{
  return vectorData()->isAvailable (m_acc.auxid());
}


/**
 * @brief Return the aux id for this variable.
 */
template <class T, class D>
SG::auxid_t WriteDecorHandle<T, D>::auxid() const
{
  return m_acc.auxid();
}


/**
 * @brief Return the mode (read/write/update) for this handle.
 */
template <class T, class D>
inline
Gaudi::DataHandle::Mode WriteDecorHandle<T, D>::mode() const
{
  return Gaudi::DataHandle::Writer;
}


/**
 * @brief Return the name of the decoration alias (CONT.DECOR).
 */
template <class T, class D>
inline
const std::string& WriteDecorHandle<T, D>::decorKey() const
{
  return m_decorKey;
}


/**
 * @brief Create the decoration if it doesn't actually exist yet.
 *        Also create the decoration alias in SG (unless this handle
 *        has been renounced).
 * @param avd The AuxVectorData instance we're decorating.
 *            (May not be the same as this->vectorData() if the
 *            handle has been renounced.)
 */
template <class T, class D>
void WriteDecorHandle<T, D>::makeDecor (const AuxVectorData* avd)
{
  if (!avd) return;
  if (avd->size_v() > 0) {
    m_acc.getDecorationSpan (*avd);
  }
  if (!m_renounced) {
    this->alias (WriteHandleKey<T> (this->m_decorKey)).ignore();
  }
  m_decorated = true;
}


/** 
 * @brief Return the referenced object as a @c SG::AuxVectorData.
 *        Specialization for the case of a standalone object
 *        (@c T derives from @c SG::AuxElement).
 */
template <class T, class D>
const SG::AuxVectorData* WriteDecorHandle<T, D>::vectorData (std::true_type)
{
  return (*this)->container();
}


/** 
 * @brief Return the referenced object as a @c SG::AuxVectorData.
 *        Specialization for the case of a container
 *        (@c T does not derive from @c SG::AuxElement).
 */
template <class T, class D>
const SG::AuxVectorData* WriteDecorHandle<T, D>::vectorData (std::false_type)
{
  return this->cptr();
}


/** 
 * @brief Return the referenced object as a @c SG::AuxVectorData.
 *
 * If @c T is a container object, then this should be the object itself.
 * But if it is a standalone object, deriving from @c SG::AuxElement,
 * then we need to call container() on the object.
 */
template <class T, class D>
const SG::AuxVectorData* WriteDecorHandle<T, D>::vectorData()
{
  return vectorData (typename std::is_base_of<SG::AuxElement, T>::type());
}


/**
 * @brief Return a @c WriteDecorHandle referencing @c key.
 * @param key The key object holding the clid/key/store.
 *
 * This will raise an exception if the StoreGate key is blank,
 * or if the event store cannot be found.
 *
 * The type of the decoration must be included as an explicit template parameter:
 *
 *@code
 *   auto handle = SG::makeHandle<float> (key);
 @endcode
 *
 * Note that @c D comes first in the argument list.  It's given explicitly,
 * while @c T is inferred from @c key.
 */
template <class D, class T>
WriteDecorHandle<T, D> makeHandle (const WriteDecorHandleKey<T>& key)
{
  return WriteDecorHandle<T, D> (key);
}


/**
 * @brief Return a @c WriteDecorHandle referencing @c key for an explicit context.
 * @param key The key object holding the clid/key/store.
 * @param ctx The event context.
 *
 * This will raise an exception if the StoreGate key is blank,
 * or if the event store cannot be found.
 *
 * If the default event store has been requested, then the thread-specific
 * store from the event context will be used.
 *
 * The type of the decoration must be included as an explicit template parameter:
 *
 *@code
 *   auto handle = SG::makeHandle<float> (key, ctx);
 @endcode
 *
 * Note that @c D comes first in the argument list.  It's given explicitly,
 * while @c T is inferred from @c key.
 */
template <class D, class T>
WriteDecorHandle<T, D> makeHandle (const WriteDecorHandleKey<T>& key,
                                   const EventContext& ctx)
{
  return WriteDecorHandle<T, D> (key, ctx);
}


/**
 * @brief These two signatures are to catch cases where the explicit
 *        template argument is omitted from the @c makeHandle call
 *        and give an error tailored to that.  Otherwise, the @c makeHandle
 *        call for @c ReadHandle would match, potentially giving a much
 *        more confusing error.
 */
template <class T>
void makeHandle (const WriteDecorHandleKey<T>& /*key*/)
{
  // If you see an error from here, you've forgotten the explicit template
  // argument to @c makeHandle giving the decoration type.
  // See the examples of @c makeHandle above.
  return T::makeHandleForDecorationsRequiresExplicitTemplateArgument();
}
template <class T>
void makeHandle (const WriteDecorHandleKey<T>& /*key*/,
                 const EventContext& /*ctx*/)
{
  // If you see an error from here, you've forgotten the explicit template
  // argument to @c makeHandle giving the decoration type.
  // See the examples of @c makeHandle above.
  return T::makeHandleForDecorationsRequiresExplicitTemplateArgument();
}


} // namespace SG
