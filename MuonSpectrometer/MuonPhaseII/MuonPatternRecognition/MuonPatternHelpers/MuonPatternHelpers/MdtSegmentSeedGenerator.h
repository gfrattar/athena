/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONR4_MUONPATTERNHELPERS_MDTSEGMENTSEEDGENERATOR_H
#define MUONR4_MUONPATTERNHELPERS_MDTSEGMENTSEEDGENERATOR_H

#include <AthenaBaseComps/AthMessaging.h>
#include <MuonSpacePoint/SpacePointPerLayerSorter.h>
#include <MuonPatternEvent/SegmentSeed.h>
#include <GaudiKernel/SystemOfUnits.h>


#include <vector>
#include <array>


namespace MuonR4 {
    class ISpacePointCalibrator;
    /** @brief Helper class to generate valid seeds for the segment fit. The generator first returns a seed
     *         directly made from the patten recogntion. Afterwards it builds seeds by lying tangent lines
     *         to a pair of drift circles. The pairing starts from the innermost & outermost layers with tubes.
     *         A valid seed must have at least 4 associated hits which are within a chi2 of 5. If two seeds
     *         within the parameter resolution are generated, then the latter one is skipped. */
    class MdtSegmentSeedGenerator: public AthMessaging {
        public:
            using HitVec = SpacePointPerLayerSorter::HitVec;
            
            /** @brief Configuration switches of the module  */
            struct Config{
                /** @brief Cut on the theta angle */
                std::array<double, 2> thetaRange{0, 180.*Gaudi::Units::deg};
                /** @brief Cut on the intercept range */
                std::array<double, 2> interceptRange{-20.*Gaudi::Units::m, 20.*Gaudi::Units::m};
                /** @brief Upper cut on the hit chi2 w.r.t. seed in order to be associated to the seed*/
                double hitPullCut{5.};
                /** @brief Try at the first time the pattern seed as candidate */
                bool startWithPattern{false};
                /** @brief How many drift circles may be on a layer to be used for seeding */
                unsigned int busyLayerLimit{2};
                /** @brief How many drift circle hits needs the seed to contain in order to be valid */
                unsigned int nMdtHitCut{3};
                /** @brief Hit cut based on the fraction of collected tube layers. 
                 *         The seed must pass the tighter of the two requirements.  */
                double nMdtLayHitCut{2./3.};
                /** @brief Once a seed with even more than initially required hits is found,
                 *         reject all following seeds with less hits */
                bool tightenHitCut{true};
                /** @brief Check whether a new seed candidate shares the same left-right solution with already accepted ones
                 *         Reject the seed if it has the same amount of hits */
                bool overlapCorridor{true};
                /** @brief Recalibrate the seed drift circles from the initial estimate  */
                bool recalibSeedCircles{false};
                /** @brief Pointer to the space point calibrator */
                const ISpacePointCalibrator* calibrator{nullptr};
                /** @brief Toggle whether the seed is rapidly refitted */
                bool fastSeedFit{true};
                /** @brief Toggle whether an initial t0 fit shall be executed */
                bool fastSegFitWithT0{false};
                /** @brief Maximum number of iterations in the fast segment fit */
                unsigned int nMaxIter{100};
                /** @brief Precision cut off in the fast segment fit */
                double precCutOff{1.e-6};
            };
            /** @brief Helper struct from a generated Mdt seed */
            struct DriftCircleSeed{
                /** @brief Seed parameters */
                SegmentFit::Parameters parameters{SegmentFit::Parameters::Zero()};
                /** @brief List of calibrated measurements */
                std::vector<std::unique_ptr<CalibratedSpacePoint>> measurements{};
                /** @brief Iterations to obtain the seed */
                unsigned int nIter{0};
                /** @brief Seed chi2 */
                double chi2{0.};
                /** @brief Pointer to the parent bucket */
                const SpacePointBucket* parentBucket{nullptr};
            };
        
        /** @brief Standard constructor taking the segmentSeed to start with and then few
         *         configuration tunes
         * @param name: Name of the Seed generator's logger
         * @param segmentSeed: Seed from which the seeds for the fit are built
         * @param configuration: Passed configuration settings of the generator */
        MdtSegmentSeedGenerator(const std::string& name,
                                const SegmentSeed* segmentSeed, 
                                const Config& configuration);

        ~MdtSegmentSeedGenerator();
        /** @brief returns the next seed in the row */
        std::optional<DriftCircleSeed> nextSeed(const EventContext& ctx);
        /** @brief Returns how many seeds have been generated */
        unsigned int numGenerated() const;
        /** @brief Returns the current seed configuration */
        const Config& config() const;
        private:
            /** @brief Sign combinations to draw the 4 lines tangent to 2 drift circles
             *         The first two are indicating whether the tangent is left/right to the
             *         first/second circle. The last sign is picking the sign of the 
             *         solution arising from the final quadratic equation. */
            using SignComboType = std::array<int, 2>;
            constexpr static std::array<SignComboType,4> s_signCombos{
                std::array{ 1, 1}, std::array{ 1,-1}, 
                std::array{-1,-1}, std::array{-1, 1},  
            };
            /** @brief Cache of all solutions seen thus far */
            struct SeedSolution{
                /** @brief: Theta of the line */
                double theta{0.};
                /** @brief Intersecpt of the line */
                double Y0{0.};
                /** @brief: Uncertainty on the slope*/
                double dTheta{0.};
                /** @brief: Uncertainty on the intercept */
                double dY0{0.};
                /** @brief Used hits in the seed */
                HitVec seedHits{};
                /** @brief Vector of radial signs of the valid hits */
                std::vector<int> solutionSigns{};

                friend std::ostream& operator<<(std::ostream& ostr, const SeedSolution& sol) {
                    return sol.print(ostr);
                }
                std::ostream& print(std::ostream& ostr) const;
            };


            /** @brief Tries to build the seed from the two hits. Fails if the solution is invalid
             *         or if the seed has already been built before
             *  @param topHit: Hit candidate from the upper layer
             *  @param bottomHit: Hit candidate from the lower layer
             *  @param sign: Object encoding whether the tangent is left / right  */
            std::optional<DriftCircleSeed> buildSeed(const EventContext& ctx,
                                                     const HoughHitType& topHit, 
                                                     const HoughHitType& bottomHit, 
                                                     const SignComboType& signs); 
            /** @brief Auxillary struct to calculate fit constants */
            struct SeedFitAuxilliaries {
                /** @brief Tube position center weigthed with inverse covariances */
                Amg::Vector3D centerOfGrav{Amg::Vector3D::Zero()};
                /** @brief Vector of inverse covariances */
                std::vector<double> invCovs{};
                /** @brief Vector of drfit signs */
                std::vector<int> driftSigns{};
                /** @brief Covariance norm */
                double covNorm{0.};
                /** @brief Expectation value of T_{z}^{2} - T_{y}^{2} */
                double T_zzyy{0.};
                /** @brief Expectation value of T_{y} * T_{z} */
                double T_yz{0.}; 
                /** @brief Expectation value of T_{z} * r  */
                double T_rz{0.};
                /** @brief Expectation value of T_{y} * r  */
                double T_ry{0.};
                /** @brief Prediced y0 given as the expection value of the radii
                 *         divided by the inverse covariance sum. */
                double fitY0{0.};
            };

            struct SeedFitAuxWithT0: public SeedFitAuxilliaries{
                /** @brief Constructor */
                SeedFitAuxWithT0(SeedFitAuxilliaries&& parent):
                    SeedFitAuxilliaries{std::move(parent)}{}
                    /** @brief Expectation value of T_{y} * v */
                    double T_vy{0.};
                    /** @brief Expectation value of T_{z} * v */
                    double T_vz{0.};
                    /** @brief Expectation value of T_{y} * a */
                    double T_ay{0.};
                    /** @brief Expectation value of T_{z} * a */
                    double T_az{0.};
                    /** @brief Expectation value of r * v */
                    double R_vr{0.};
                    /** @brief Expectation value of v * v */
                    double R_vv{0.};
                    /** @brief Expectation value of r * a */
                    double R_va{0.};
                    /** @brief First derivative of the fitted Y0 */
                    double fitY0Prime{0.};
                    /** @brief Second derivative of the ftted Y0 */
                    double fitY0TwoPrime{0.};
            };

            /** @brief Helper function to estimate the auxillary variables that remain constant
             *         during the fit.
             *  @param seed: Reference to the seed to calculate the variables from */
            SeedFitAuxilliaries estimateAuxillaries(const DriftCircleSeed& seed) const;  
            /** @brief Helper function to estimate the auxillary variables that remain constants
             *          during the fit with t0
             *  @param ctx: EventContext to recalibrate the hits
             *  @param seed: Reference to the seed to calculate the variables from */
            SeedFitAuxWithT0 estimateAuxillaries(const EventContext& ctx,
                                                 const DriftCircleSeed& seed) const;          
            /** @brief Refine the seed by performing a fast Mdt segment fit. 
             *  @param seed: Seed built from the tangent adjacent to the two seed circles */
            void fitDriftCircles(DriftCircleSeed& seed) const;
            /** @brief Refine the seed by performing a fast Mdt segment fit with t0 constraint
             *  @param ctx: EventContext to recalibrate the hits
             *  @param seed: Seed built from the tangent adjacent to the two seed circles */
            void fitDriftCirclesWithT0(const EventContext& ctx,
                                       DriftCircleSeed& seed) const;            
            /** @brief Prepares the generator to generate the seed from the next pair of drift circles */
            void moveToNextCandidate();
            Config m_cfg{};
            

            const SegmentSeed* m_segmentSeed{nullptr};
            SpacePointPerLayerSorter m_hitLayers{m_segmentSeed->getHitsInMax()};
            
            /** @brief Considered layer to pick the top drift circle from*/
            std::size_t m_upperLayer{0};
            /** @brief Considered layer to pick the bottom drift circle from*/
            std::size_t m_lowerLayer{0}; 
            /** @brief Explicit hit to pick in the selected bottom layer */
            std::size_t m_lowerHitIndex{0};
            /** @brief Explicit hit to pick in the selected top layer */
            std::size_t m_upperHitIndex{0};
            /** @brief Index of the left-right ambiguity between the circles */
            std::size_t m_signComboIndex{0};
            /** @brief Vector caching equivalent solutions to avoid double seeding */
            std::vector<SeedSolution> m_seenSolutions{};
            /** Counter on how many seeds have been generated */
            unsigned int m_nGenSeeds{0};
        
    };
}

#endif
