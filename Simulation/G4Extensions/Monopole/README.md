# Monopole
Author Andrea Dell'Acqua andrea.Dellacqua@cern.ch
Converted from packagedoc.h

## Introduction

This package allows users to introduce magnetic monopoles into the Geant4 simulation.

## Class Overview
  The Monopole package contains the following classes:

  - G4Monopole : Defines the magnetic monopole particle itself
  - MonopoleProcessDefinition : Defines which processes are to be applied to the magnetic monopoles as they propagate through the detector.
  - G4mplIonisation , G4mplIonisationModel , G4mplTransportation : Defines several specific processes for the magnetic monopoles.
  


doc/mainpage.h
Monopole/G4Monopole.h
Monopole/G4mplTransportation.h
Monopole/MonopoleProcessDefinition.h

